----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:41:31 03/21/2013 
-- Design Name: 
-- Module Name:    PWM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PulseWidth is
        Port(
                        clk : in STD_LOGIC;
                        --clk50Hz : out STD_LOGIC;
                        PWM : out STD_LOGIC;
                        RST : in STD_LOGIC;
                        --btnl : in STD_LOGIC;
                        Duty_in : in integer
                        --btnr : in STD_LOGIC
                  );
end PulseWidth;

architecture Behavioral of PulseWidth is
constant Hz50 : integer := 2000000;
constant duty_min : integer := 100000;
constant duty_max : integer := 300000;
--constant duty_step : integer := 100;



signal PWMint,PWMInt_next : STD_LOGIC;
signal duty_cycle : integer;
signal duty_cycleInt : integer := duty_cycle/100;
signal duty_cycle_next : integer :=0;
signal counter,counter_next : integer :=0;
signal ping : STD_LOGIC;


begin

duty_set : process (ping)
begin
	if ping = '1' then
		if Duty_in < duty_min then
			duty_cycle_next <= duty_min;
		elsif Duty_in > duty_max then
			duty_cycle_next <= duty_max;
		else
			duty_cycle_next <= Duty_in;
		end if;
	end if;
end process;

--duty_out <= duty_cycle;
PWMer : process(clk,rst)
begin
        if rst = '1' then
                PWMInt <= '0';
                counter <= 0;
                duty_cycle <= 0;
        elsif rising_edge(clk) then
                pwmInt <= PWMint_next;
                duty_cycle <= duty_cycle_next;
                counter <= counter_next;
        end if;
end process;

counter_next <= 0 when counter = Hz50 else
                                        counter + 1;
                                        
ping <= '1' when counter = 0 else
                  '0';

--duty_cycler : process(btnl,btnr,ping,duty_cycle)
--begin
--                duty_cycle_next <= duty_cycle;
--                if ping = '1' then
--                        if btnl = '1' and duty_cycle>duty_min then
--                                duty_cycle_next <= duty_cycle - duty_step;
--                                duty_cycleInt <= duty_cycle_next;
--                        elsif btnr = '1' and duty_cycle<duty_max then
--                                duty_cycle_next <= duty_cycle + duty_step;
--                                duty_cycleInt <= duty_cycle_next;
--                        end if;
--                end if;
--end process duty_cycler;

pwm <= pwmInt;
pwmint_next <= '1' when counter < duty_cycle else
                                        '0';
end Behavioral;