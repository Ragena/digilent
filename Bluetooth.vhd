----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:30:22 03/19/2013 
-- Design Name: 
-- Module Name:    Bluetooth - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bluetooth is
    Port (
			  clk100Mhz : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  BtEnSW : in STD_LOGIC;
			  BtEn : out STD_LOGIC;
			  Rx  : in STD_LOGIC;
			  Tx  : out STD_LOGIC;
			  STATUS : in STD_LOGIC;
			  --DataTxo : out STD_LOGIC;
			  LED : out STD_LOGIC_VECTOR(7 downto 0);
			 -- DataRxo : out STD_LOGIC_VECTOR(7 downto 0)
			  Datasw : in STD_LOGIC
			  
			  
	 );
end Bluetooth;

architecture Behavioral of Bluetooth is
----------------------------------------------------------
--Universal constants
----------------------------------------------------------
constant databits : integer := 8;
signal BtEnInt : STD_LOGIC := '1';

----------------------------------------------------------
--BAUD GENERATION : 115200
----------------------------------------------------------
constant Baud_clkdiv_Tx : integer := 868;
constant Baud_clkdiv_Rx : integer := 54;
signal Baud_counter_Tx : integer;
signal Baud_counter_Rx : integer;
signal Baud_signal_Tx : STD_LOGIC := '0';
signal Baud_signal_Rx : STD_LOGIC := '0';
----------------------------------------------------------
--Transmission
----------------------------------------------------------
 type Tx_States is (idle, wait_for_tick, send_start_bit,transmit_data,send_stop_bit);
 signal Tx_State : Tx_States := idle;
 signal DataTx : STD_LOGIC_VECTOR(7 downto 0) := "01010100";
 signal DataT : STD_LOGIC := '1';
 signal Tx_counter : integer := 0;
 signal Rx_ack : STD_LOGIC := '0';

----------------------------------------------------------
--Receiver
----------------------------------------------------------

type Rx_States is (get_start_bit,get_data,End_Rx);
signal Rx_state : Rx_states := get_start_bit;
signal DataR : STD_LOGIC := '0';
signal DataRx: STD_LOGIC_VECTOR(7 downto 0) := X"00";
signal Rx_counter, Rx_space_counter : integer := 0;
signal Rx_data_sync : STD_LOGIC_VECTOR(1 downto 0):= "00";
signal Rx_filter : integer := 0;
signal Rx_spacing : STD_LOGIC := '0';


----------------------------------------------------------
--PROGRAM BEGIN
----------------------------------------------------------
begin
--DataRxo <= DataRx;
BtEn <= BtEnSW;
Tx <= DataT;
--LED(0) <= STATUS;
--LED <= DataRx;
----------------------------------------------------------
--Transmitter clock
----------------------------------------------------------
TX_CLOCK : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		if rst = '1' then
			Baud_counter_Tx <= 0;
			Baud_signal_Tx <= '0';
		else
			if Baud_counter_Tx = Baud_clkdiv_Tx then
				Baud_counter_Tx <= 0;
				Baud_signal_Tx <= '1';
			else
				Baud_counter_Tx <= Baud_counter_Tx + 1;
				Baud_signal_Tx <= '0';
			end if;
		end if;
	end if;
end process TX_CLOCK;



---------------------------------------------------------
--Transmitter
---------------------------------------------------------
TX_DATASEND : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		if rst = '1' then
			DataT <= '1';
			DataTx <= X"00";
			Tx_counter <= 0;
			Tx_State <= idle;
			Rx_ack <= '0';
		else
			Rx_ack <= '0';
			case Tx_state is
				when idle =>
					if DataSW = '1' then
						Tx_state <= wait_for_tick;
					else
						Tx_state <= idle;
						--LED(7) <= '1';
						--LED(6 downto 3) <= X"0";
					end if;
				when wait_for_tick =>
					--LED(6) <= '1';
					--LED(7) <= '0';
					--LED(5 downto 3) <= "000";
					if Baud_signal_Tx = '1' then
						Tx_state <= send_start_bit;
					end if;
				when send_start_bit =>
					--LED(5) <= '1';
					--LED(7 downto 6) <= "00";
					--LED(4 downto 3) <= "00";
					if Baud_signal_Tx = '1' then
						DataT <= '0';
						Tx_state <= transmit_data;
						Tx_counter <= 0;
					end if;
				when transmit_data =>
					--LED(4) <= '1';
					--LED(7 downto 5) <= "000";
					--LED(3) <= '0';
					if Baud_signal_Tx = '1' then
						if Tx_counter < 7 then
							DataT <= DataTx(Tx_counter);
							Tx_counter <= Tx_counter + 1;
						else
							DataT <= DataTx(7);
							Tx_counter <= 0;
							Tx_state <= send_stop_bit;
						end if;
					end if;
				when send_stop_bit =>
					--LED(3) <= '1';
					--LED(7 downto 4) <= "0000";
					if Baud_signal_Tx = '1' then
						DataT <= '1';
						Tx_state <= idle;
					end if;
				when others =>
					DataT <= '1';
					Tx_state <= idle;
			end case;
		end if;
	end if;
end process TX_DATASEND;
							

---------------------------------------------------------
--Receiver clock
---------------------------------------------------------
RX_CLOCK : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		if rst = '1' then
			Baud_counter_Rx <= 0;
			Baud_signal_Rx <= '0';
		else
			if Baud_counter_Rx = Baud_clkdiv_Rx then
				Baud_counter_Rx <= 0;
				Baud_signal_Rx <= '1';
			else
				Baud_counter_Rx <= Baud_counter_Rx + 1;
				Baud_signal_Rx <= '0';
			end if;
		end if;
	end if;
end process RX_CLOCK;

---------------------------------------------------------
--Rx Synchronize
---------------------------------------------------------

RX_SYNC : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		if rst = '1' then
			Rx_Data_sync <= "00";
		else
			if Baud_signal_Rx = '1' then
				Rx_Data_Sync(0) <= Rx;
				Rx_Data_Sync(1) <= Rx_Data_Sync(0);
			end if;
		end if;
	end if;
end process RX_SYNC;



---------------------------------------------------------
--Rx Filter
---------------------------------------------------------
RX_FILTERER : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		if rst = '1' then
			Rx_filter <= 0;
			DataR <= '1';
		else
			if Baud_signal_Rx = '1' then
				if Rx_Data_sync(1) = '1' and Rx_filter < 3 then
					Rx_filter <= Rx_filter + 1;
				elsif Rx_Data_sync(1) = '0' and Rx_filter > 0 then
					Rx_filter <= Rx_filter - 1;
				end if;
				if Rx_filter = 3 then
					DataR <= '1';
				elsif Rx_filter = 0 then
					DataR <= '0';
				end if;
			end if;
		end if;
	end if;
end process RX_FILTERER;

---------------------------------------------------------
--RX Bit Spacing
---------------------------------------------------------
RX_BIT_SPACE : process(clk100Mhz)
begin
	if rising_edge(clk100Mhz) then
		Rx_Spacing <= '0';
		if Baud_signal_Rx = '1' then
			if Rx_space_counter = 15 then
				Rx_Spacing <= '1';
				Rx_space_counter <= 0;
			else
				Rx_space_counter <= Rx_space_counter + 1;
			end if;
		end if;
	end if;
end process RX_BIT_SPACE;
		
			


---------------------------------------------------------
--Data Rx - Receiver
---------------------------------------------------------


RX_Receive : process(Baud_signal_Rx)
begin
	if rising_edge(Baud_signal_Rx) then
		if rst = '1' then
			DataRx <= X"00";
			Rx_counter <= 0;
			Rx_state <= get_start_bit;
		else
			case Rx_state is
				when get_start_bit =>
					if DataR = '0' and Baud_signal_Rx = '1' then
						Rx_state <= get_data;
						--LED(6) <= '1';
					else
						Rx_state <= get_start_bit;
						--LED(7) <= '1';
					end if;
				when get_data =>
					if Baud_signal_Rx = '1' then
						if Rx_counter < databits  then
							DataRx(Rx_counter) <= Rx;
							Rx_counter <= Rx_counter + 1;
						--LED(7) <= '0';
						--LED(6) <= '0';
						--LED(5) <= '1';
						else
							Rx_state <= End_Rx;
							Rx_counter <= 0;
						--LED(4) <= '1';
						end if;
					end if;
				when End_Rx =>
					Rx_state <= get_start_bit;
				when others =>
					--LED(0) <= '1';
			end case;
		end if;
	end if;
end process RX_Receive;

--Test : process(DataRx)
--begin
--	if Rx_signal = '1' then
--		if DataRx = "01001100" then
--			LED(7) <= '1';
--		elsif DataRx = "01001111" then
--			LED(7) <= '0';
--		end if;
--	end if;
--end process Test;
end Behavioral;

