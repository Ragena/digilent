
-- VHDL Instantiation Created from source file PulseWidth.vhd -- 18:32:52 04/19/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT PulseWidth
	PORT(
		clk : IN std_logic;
		RST : IN std_logic;
		Duty_in : IN std_logic_vector(0 to 18);
		btnl : IN std_logic;
		btnr : IN std_logic;          
		clk50Hz : OUT std_logic;
		PWM : OUT std_logic;
		Duty_out : OUT std_logic
		);
	END COMPONENT;

	Inst_PulseWidth: PulseWidth PORT MAP(
		clk => ,
		clk50Hz => ,
		PWM => ,
		RST => ,
		Duty_in => ,
		btnl => ,
		Duty_out => ,
		btnr => 
	);


