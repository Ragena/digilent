--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: PmodGYRO_Demo_synthesis.vhd
-- /___/   /\     Timestamp: Fri Apr 19 18:33:18 2013
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm PmodGYRO_Demo -w -dir netgen/synthesis -ofmt vhdl -sim PmodGYRO_Demo.ngc PmodGYRO_Demo_synthesis.vhd 
-- Device	: xc6slx16-3-csg324
-- Input file	: PmodGYRO_Demo.ngc
-- Output file	: /media/andu/Stuff/DDC/anduProovid/GYRO/GYRO/netgen/synthesis/PmodGYRO_Demo_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: PmodGYRO_Demo
-- Xilinx	: /opt/Xilinx/14.4/ISE_DS/ISE/
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity PmodGYRO_Demo is
  port (
    rst : in STD_LOGIC := 'X'; 
    clk : in STD_LOGIC := 'X'; 
    start : in STD_LOGIC := 'X'; 
    JA : inout STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end PmodGYRO_Demo;

architecture Structure of PmodGYRO_Demo is
  signal rst_IBUF_0 : STD_LOGIC; 
  signal clk_BUFGP_1 : STD_LOGIC; 
  signal start_IBUF_2 : STD_LOGIC; 
  signal JA_2_IBUF_3 : STD_LOGIC; 
  signal C0_send_data_7_Q : STD_LOGIC; 
  signal C0_send_data_6_Q : STD_LOGIC; 
  signal C0_send_data_5_Q : STD_LOGIC; 
  signal C0_send_data_3_Q : STD_LOGIC; 
  signal C0_send_data_1_Q : STD_LOGIC; 
  signal C0_send_data_0_Q : STD_LOGIC; 
  signal C0_begin_transmission_10 : STD_LOGIC; 
  signal C0_slave_select_11 : STD_LOGIC; 
  signal C1_end_transmission_12 : STD_LOGIC; 
  signal C1_mosi_13 : STD_LOGIC; 
  signal C1_sclk_previous_14 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT511 : STD_LOGIC; 
  signal C0_n0297_inv2 : STD_LOGIC; 
  signal C0_Reset_OR_DriverANDClockEnable24 : STD_LOGIC; 
  signal C0_Reset_OR_DriverANDClockEnable : STD_LOGIC; 
  signal C0_Result_11_1 : STD_LOGIC; 
  signal C0_Result_10_1 : STD_LOGIC; 
  signal C0_Result_9_1 : STD_LOGIC; 
  signal C0_Result_8_1 : STD_LOGIC; 
  signal C0_Result_7_1 : STD_LOGIC; 
  signal C0_Result_6_1 : STD_LOGIC; 
  signal C0_Result_5_1 : STD_LOGIC; 
  signal C0_Result_4_1 : STD_LOGIC; 
  signal C0_Result_3_1 : STD_LOGIC; 
  signal C0_Result_2_1 : STD_LOGIC; 
  signal C0_Result_1_1 : STD_LOGIC; 
  signal C0_Result_0_1 : STD_LOGIC; 
  signal C0_n0460_inv : STD_LOGIC; 
  signal C0_n0357_inv : STD_LOGIC; 
  signal C0_ss_count_11_PWR_7_o_equal_117_o : STD_LOGIC; 
  signal C0_count_wait_23_GND_7_o_equal_122_o : STD_LOGIC; 
  signal C0_n0273_3_Q : STD_LOGIC; 
  signal C0_n0273_5_Q : STD_LOGIC; 
  signal C0_n0273_6_Q : STD_LOGIC; 
  signal C0_n0273_7_Q : STD_LOGIC; 
  signal C0_STATE_2_X_7_o_Mux_127_o : STD_LOGIC; 
  signal C0_STATE_2_X_7_o_wide_mux_130_OUT_0_Q : STD_LOGIC; 
  signal C0_STATE_2_X_7_o_wide_mux_130_OUT_1_Q : STD_LOGIC; 
  signal C0_STATE_2_X_7_o_wide_mux_130_OUT_2_Q : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011 : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151 : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102 : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153 : STD_LOGIC; 
  signal C1_Reset_OR_DriverANDClockEnable : STD_LOGIC; 
  signal C1_RxTxSTATE_FSM_FFd1_In : STD_LOGIC; 
  signal C1_RxTxSTATE_FSM_FFd2_In : STD_LOGIC; 
  signal C1_rst_inv : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_0_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_1_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_2_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_3_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_0_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_1_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_2_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_3_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_4_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_5_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_6_Q : STD_LOGIC; 
  signal C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_7_Q : STD_LOGIC; 
  signal C1_sclk_buffer_198 : STD_LOGIC; 
  signal C1_RxTxSTATE_FSM_FFd2_219 : STD_LOGIC; 
  signal C1_RxTxSTATE_FSM_FFd1_220 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT5 : STD_LOGIC; 
  signal C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT51_223 : STD_LOGIC; 
  signal C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT3 : STD_LOGIC; 
  signal C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT32 : STD_LOGIC; 
  signal C0_ss_count_11_PWR_7_o_equal_117_o_11_Q : STD_LOGIC; 
  signal C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227 : STD_LOGIC; 
  signal C0_count_wait_23_GND_7_o_equal_122_o_23_Q : STD_LOGIC; 
  signal C0_count_wait_23_GND_7_o_equal_122_o_23_1_229 : STD_LOGIC; 
  signal C0_count_wait_23_GND_7_o_equal_122_o_23_2_230 : STD_LOGIC; 
  signal C0_count_wait_23_GND_7_o_equal_122_o_23_3_231 : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT61 : STD_LOGIC; 
  signal C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT8 : STD_LOGIC; 
  signal C1_Reset_OR_DriverANDClockEnable11_234 : STD_LOGIC; 
  signal C1_Reset_OR_DriverANDClockEnable12_235 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N8 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal C1_n0119_inv2_242 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_10_rt_250 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_9_rt_251 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_8_rt_252 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_7_rt_253 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_6_rt_254 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_5_rt_255 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_4_rt_256 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_3_rt_257 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_2_rt_258 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy_1_rt_259 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_21_rt_260 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_20_rt_261 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_19_rt_262 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_18_rt_263 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_17_rt_264 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_16_rt_265 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_15_rt_266 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_14_rt_267 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_13_rt_268 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_12_rt_269 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_11_rt_270 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_10_rt_271 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_9_rt_272 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_8_rt_273 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_7_rt_274 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_6_rt_275 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_5_rt_276 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_4_rt_277 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_3_rt_278 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_2_rt_279 : STD_LOGIC; 
  signal C0_Mcount_count_wait_cy_1_rt_280 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_10_rt_281 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_9_rt_282 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_8_rt_283 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_7_rt_284 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_6_rt_285 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_5_rt_286 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_4_rt_287 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_3_rt_288 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_2_rt_289 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_cy_1_rt_290 : STD_LOGIC; 
  signal C0_Mcount_ss_count_xor_11_rt_291 : STD_LOGIC; 
  signal C0_Mcount_count_wait_xor_22_rt_292 : STD_LOGIC; 
  signal C1_Mcount_spi_clk_count_xor_11_rt_293 : STD_LOGIC; 
  signal C0_slave_select_rstpot_294 : STD_LOGIC; 
  signal C1_mosi_rstpot_295 : STD_LOGIC; 
  signal C1_sclk_previous_rstpot_296 : STD_LOGIC; 
  signal C1_sclk_buffer_rstpot_297 : STD_LOGIC; 
  signal C0_begin_transmission_rstpot_298 : STD_LOGIC; 
  signal C1_end_transmission_rstpot_299 : STD_LOGIC; 
  signal N24 : STD_LOGIC; 
  signal N26 : STD_LOGIC; 
  signal N27 : STD_LOGIC; 
  signal N29 : STD_LOGIC; 
  signal N31 : STD_LOGIC; 
  signal C0_count_wait_0_rstpot_305 : STD_LOGIC; 
  signal N33 : STD_LOGIC; 
  signal N34 : STD_LOGIC; 
  signal N36 : STD_LOGIC; 
  signal N37 : STD_LOGIC; 
  signal N45 : STD_LOGIC; 
  signal N46 : STD_LOGIC; 
  signal N47 : STD_LOGIC; 
  signal N48 : STD_LOGIC; 
  signal N49 : STD_LOGIC; 
  signal N51 : STD_LOGIC; 
  signal C0_count_wait_22_rstpot_316 : STD_LOGIC; 
  signal C0_count_wait_21_rstpot_317 : STD_LOGIC; 
  signal C0_count_wait_20_rstpot_318 : STD_LOGIC; 
  signal C0_count_wait_19_rstpot_319 : STD_LOGIC; 
  signal C0_count_wait_18_rstpot_320 : STD_LOGIC; 
  signal C0_count_wait_17_rstpot_321 : STD_LOGIC; 
  signal C0_count_wait_16_rstpot_322 : STD_LOGIC; 
  signal C0_count_wait_15_rstpot_323 : STD_LOGIC; 
  signal C0_count_wait_14_rstpot_324 : STD_LOGIC; 
  signal C0_count_wait_13_rstpot_325 : STD_LOGIC; 
  signal C0_count_wait_12_rstpot_326 : STD_LOGIC; 
  signal C0_count_wait_11_rstpot_327 : STD_LOGIC; 
  signal C0_count_wait_10_rstpot_328 : STD_LOGIC; 
  signal C0_count_wait_9_rstpot_329 : STD_LOGIC; 
  signal C0_count_wait_8_rstpot_330 : STD_LOGIC; 
  signal C0_count_wait_7_rstpot_331 : STD_LOGIC; 
  signal C0_count_wait_6_rstpot_332 : STD_LOGIC; 
  signal C0_count_wait_5_rstpot_333 : STD_LOGIC; 
  signal C0_count_wait_4_rstpot_334 : STD_LOGIC; 
  signal C0_count_wait_3_rstpot_335 : STD_LOGIC; 
  signal C0_count_wait_2_rstpot_336 : STD_LOGIC; 
  signal C0_count_wait_1_rstpot_337 : STD_LOGIC; 
  signal C0_STATE_1_1_338 : STD_LOGIC; 
  signal C0_STATE_2_1_339 : STD_LOGIC; 
  signal C0_n0297_inv1_rstpot_340 : STD_LOGIC; 
  signal C0_send_data_0_dpot_341 : STD_LOGIC; 
  signal C0_send_data_1_dpot_342 : STD_LOGIC; 
  signal C0_send_data_3_dpot_343 : STD_LOGIC; 
  signal C0_send_data_5_dpot_344 : STD_LOGIC; 
  signal C0_send_data_6_dpot_345 : STD_LOGIC; 
  signal C0_send_data_7_dpot_346 : STD_LOGIC; 
  signal C0_STATE_1_2_347 : STD_LOGIC; 
  signal C1_spi_clk_count_11_rstpot_348 : STD_LOGIC; 
  signal C1_spi_clk_count_10_rstpot_349 : STD_LOGIC; 
  signal C1_spi_clk_count_9_rstpot_350 : STD_LOGIC; 
  signal C1_spi_clk_count_8_rstpot_351 : STD_LOGIC; 
  signal C1_spi_clk_count_7_rstpot_352 : STD_LOGIC; 
  signal C1_spi_clk_count_6_rstpot_353 : STD_LOGIC; 
  signal C1_spi_clk_count_5_rstpot_354 : STD_LOGIC; 
  signal C1_spi_clk_count_4_rstpot_355 : STD_LOGIC; 
  signal C1_spi_clk_count_3_rstpot_356 : STD_LOGIC; 
  signal C1_spi_clk_count_2_rstpot_357 : STD_LOGIC; 
  signal C1_spi_clk_count_1_rstpot_358 : STD_LOGIC; 
  signal C1_spi_clk_count_0_rstpot_359 : STD_LOGIC; 
  signal C0_Reset_OR_DriverANDClockEnable361_360 : STD_LOGIC; 
  signal C1_RxTxSTATE_FSM_FFd2_1_361 : STD_LOGIC; 
  signal C0_ss_count_11_rstpot_362 : STD_LOGIC; 
  signal C0_ss_count_10_rstpot_363 : STD_LOGIC; 
  signal C0_ss_count_9_rstpot_364 : STD_LOGIC; 
  signal C0_ss_count_8_rstpot_365 : STD_LOGIC; 
  signal C0_ss_count_7_rstpot_366 : STD_LOGIC; 
  signal C0_ss_count_6_rstpot_367 : STD_LOGIC; 
  signal C0_ss_count_5_rstpot_368 : STD_LOGIC; 
  signal C0_ss_count_4_rstpot_369 : STD_LOGIC; 
  signal C0_ss_count_3_rstpot_370 : STD_LOGIC; 
  signal C0_ss_count_2_rstpot_371 : STD_LOGIC; 
  signal C0_ss_count_1_rstpot_372 : STD_LOGIC; 
  signal C0_ss_count_0_rstpot_373 : STD_LOGIC; 
  signal N53 : STD_LOGIC; 
  signal N54 : STD_LOGIC; 
  signal C0_Mcount_ss_count_cy : STD_LOGIC_VECTOR ( 10 downto 0 ); 
  signal C0_Mcount_ss_count_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal C0_Mcount_count_wait_cy : STD_LOGIC_VECTOR ( 21 downto 0 ); 
  signal C0_Mcount_count_wait_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal C0_byte_count : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal C0_Result : STD_LOGIC_VECTOR ( 22 downto 0 ); 
  signal C0_n0452 : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal C0_n0343 : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal C0_previousSTATE : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal C0_ss_count : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal C0_count_wait : STD_LOGIC_VECTOR ( 23 downto 0 ); 
  signal C0_STATE : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal C1_Mcount_spi_clk_count_cy : STD_LOGIC_VECTOR ( 10 downto 0 ); 
  signal C1_Mcount_spi_clk_count_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal C1_rx_count : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal C1_Result : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal C1_spi_clk_count : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal C1_shift_register : STD_LOGIC_VECTOR ( 7 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => C0_count_wait(23)
    );
  XST_VCC : VCC
    port map (
      P => N1
    );
  C0_Mcount_ss_count_xor_11_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(10),
      LI => C0_Mcount_ss_count_xor_11_rt_291,
      O => C0_Result_11_1
    );
  C0_Mcount_ss_count_xor_10_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(9),
      LI => C0_Mcount_ss_count_cy_10_rt_250,
      O => C0_Result_10_1
    );
  C0_Mcount_ss_count_cy_10_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(9),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_10_rt_250,
      O => C0_Mcount_ss_count_cy(10)
    );
  C0_Mcount_ss_count_xor_9_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(8),
      LI => C0_Mcount_ss_count_cy_9_rt_251,
      O => C0_Result_9_1
    );
  C0_Mcount_ss_count_cy_9_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(8),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_9_rt_251,
      O => C0_Mcount_ss_count_cy(9)
    );
  C0_Mcount_ss_count_xor_8_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(7),
      LI => C0_Mcount_ss_count_cy_8_rt_252,
      O => C0_Result_8_1
    );
  C0_Mcount_ss_count_cy_8_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(7),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_8_rt_252,
      O => C0_Mcount_ss_count_cy(8)
    );
  C0_Mcount_ss_count_xor_7_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(6),
      LI => C0_Mcount_ss_count_cy_7_rt_253,
      O => C0_Result_7_1
    );
  C0_Mcount_ss_count_cy_7_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(6),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_7_rt_253,
      O => C0_Mcount_ss_count_cy(7)
    );
  C0_Mcount_ss_count_xor_6_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(5),
      LI => C0_Mcount_ss_count_cy_6_rt_254,
      O => C0_Result_6_1
    );
  C0_Mcount_ss_count_cy_6_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(5),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_6_rt_254,
      O => C0_Mcount_ss_count_cy(6)
    );
  C0_Mcount_ss_count_xor_5_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(4),
      LI => C0_Mcount_ss_count_cy_5_rt_255,
      O => C0_Result_5_1
    );
  C0_Mcount_ss_count_cy_5_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(4),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_5_rt_255,
      O => C0_Mcount_ss_count_cy(5)
    );
  C0_Mcount_ss_count_xor_4_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(3),
      LI => C0_Mcount_ss_count_cy_4_rt_256,
      O => C0_Result_4_1
    );
  C0_Mcount_ss_count_cy_4_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(3),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_4_rt_256,
      O => C0_Mcount_ss_count_cy(4)
    );
  C0_Mcount_ss_count_xor_3_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(2),
      LI => C0_Mcount_ss_count_cy_3_rt_257,
      O => C0_Result_3_1
    );
  C0_Mcount_ss_count_cy_3_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(2),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_3_rt_257,
      O => C0_Mcount_ss_count_cy(3)
    );
  C0_Mcount_ss_count_xor_2_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(1),
      LI => C0_Mcount_ss_count_cy_2_rt_258,
      O => C0_Result_2_1
    );
  C0_Mcount_ss_count_cy_2_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(1),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_2_rt_258,
      O => C0_Mcount_ss_count_cy(2)
    );
  C0_Mcount_ss_count_xor_1_Q : XORCY
    port map (
      CI => C0_Mcount_ss_count_cy(0),
      LI => C0_Mcount_ss_count_cy_1_rt_259,
      O => C0_Result_1_1
    );
  C0_Mcount_ss_count_cy_1_Q : MUXCY
    port map (
      CI => C0_Mcount_ss_count_cy(0),
      DI => C0_count_wait(23),
      S => C0_Mcount_ss_count_cy_1_rt_259,
      O => C0_Mcount_ss_count_cy(1)
    );
  C0_Mcount_ss_count_xor_0_Q : XORCY
    port map (
      CI => C0_count_wait(23),
      LI => C0_Mcount_ss_count_lut(0),
      O => C0_Result_0_1
    );
  C0_Mcount_ss_count_cy_0_Q : MUXCY
    port map (
      CI => C0_count_wait(23),
      DI => N1,
      S => C0_Mcount_ss_count_lut(0),
      O => C0_Mcount_ss_count_cy(0)
    );
  C0_Mcount_count_wait_xor_22_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(21),
      LI => C0_Mcount_count_wait_xor_22_rt_292,
      O => C0_Result(22)
    );
  C0_Mcount_count_wait_xor_21_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(20),
      LI => C0_Mcount_count_wait_cy_21_rt_260,
      O => C0_Result(21)
    );
  C0_Mcount_count_wait_cy_21_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(20),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_21_rt_260,
      O => C0_Mcount_count_wait_cy(21)
    );
  C0_Mcount_count_wait_xor_20_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(19),
      LI => C0_Mcount_count_wait_cy_20_rt_261,
      O => C0_Result(20)
    );
  C0_Mcount_count_wait_cy_20_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(19),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_20_rt_261,
      O => C0_Mcount_count_wait_cy(20)
    );
  C0_Mcount_count_wait_xor_19_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(18),
      LI => C0_Mcount_count_wait_cy_19_rt_262,
      O => C0_Result(19)
    );
  C0_Mcount_count_wait_cy_19_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(18),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_19_rt_262,
      O => C0_Mcount_count_wait_cy(19)
    );
  C0_Mcount_count_wait_xor_18_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(17),
      LI => C0_Mcount_count_wait_cy_18_rt_263,
      O => C0_Result(18)
    );
  C0_Mcount_count_wait_cy_18_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(17),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_18_rt_263,
      O => C0_Mcount_count_wait_cy(18)
    );
  C0_Mcount_count_wait_xor_17_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(16),
      LI => C0_Mcount_count_wait_cy_17_rt_264,
      O => C0_Result(17)
    );
  C0_Mcount_count_wait_cy_17_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(16),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_17_rt_264,
      O => C0_Mcount_count_wait_cy(17)
    );
  C0_Mcount_count_wait_xor_16_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(15),
      LI => C0_Mcount_count_wait_cy_16_rt_265,
      O => C0_Result(16)
    );
  C0_Mcount_count_wait_cy_16_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(15),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_16_rt_265,
      O => C0_Mcount_count_wait_cy(16)
    );
  C0_Mcount_count_wait_xor_15_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(14),
      LI => C0_Mcount_count_wait_cy_15_rt_266,
      O => C0_Result(15)
    );
  C0_Mcount_count_wait_cy_15_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(14),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_15_rt_266,
      O => C0_Mcount_count_wait_cy(15)
    );
  C0_Mcount_count_wait_xor_14_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(13),
      LI => C0_Mcount_count_wait_cy_14_rt_267,
      O => C0_Result(14)
    );
  C0_Mcount_count_wait_cy_14_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(13),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_14_rt_267,
      O => C0_Mcount_count_wait_cy(14)
    );
  C0_Mcount_count_wait_xor_13_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(12),
      LI => C0_Mcount_count_wait_cy_13_rt_268,
      O => C0_Result(13)
    );
  C0_Mcount_count_wait_cy_13_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(12),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_13_rt_268,
      O => C0_Mcount_count_wait_cy(13)
    );
  C0_Mcount_count_wait_xor_12_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(11),
      LI => C0_Mcount_count_wait_cy_12_rt_269,
      O => C0_Result(12)
    );
  C0_Mcount_count_wait_cy_12_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(11),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_12_rt_269,
      O => C0_Mcount_count_wait_cy(12)
    );
  C0_Mcount_count_wait_xor_11_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(10),
      LI => C0_Mcount_count_wait_cy_11_rt_270,
      O => C0_Result(11)
    );
  C0_Mcount_count_wait_cy_11_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(10),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_11_rt_270,
      O => C0_Mcount_count_wait_cy(11)
    );
  C0_Mcount_count_wait_xor_10_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(9),
      LI => C0_Mcount_count_wait_cy_10_rt_271,
      O => C0_Result(10)
    );
  C0_Mcount_count_wait_cy_10_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(9),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_10_rt_271,
      O => C0_Mcount_count_wait_cy(10)
    );
  C0_Mcount_count_wait_xor_9_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(8),
      LI => C0_Mcount_count_wait_cy_9_rt_272,
      O => C0_Result(9)
    );
  C0_Mcount_count_wait_cy_9_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(8),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_9_rt_272,
      O => C0_Mcount_count_wait_cy(9)
    );
  C0_Mcount_count_wait_xor_8_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(7),
      LI => C0_Mcount_count_wait_cy_8_rt_273,
      O => C0_Result(8)
    );
  C0_Mcount_count_wait_cy_8_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(7),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_8_rt_273,
      O => C0_Mcount_count_wait_cy(8)
    );
  C0_Mcount_count_wait_xor_7_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(6),
      LI => C0_Mcount_count_wait_cy_7_rt_274,
      O => C0_Result(7)
    );
  C0_Mcount_count_wait_cy_7_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(6),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_7_rt_274,
      O => C0_Mcount_count_wait_cy(7)
    );
  C0_Mcount_count_wait_xor_6_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(5),
      LI => C0_Mcount_count_wait_cy_6_rt_275,
      O => C0_Result(6)
    );
  C0_Mcount_count_wait_cy_6_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(5),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_6_rt_275,
      O => C0_Mcount_count_wait_cy(6)
    );
  C0_Mcount_count_wait_xor_5_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(4),
      LI => C0_Mcount_count_wait_cy_5_rt_276,
      O => C0_Result(5)
    );
  C0_Mcount_count_wait_cy_5_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(4),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_5_rt_276,
      O => C0_Mcount_count_wait_cy(5)
    );
  C0_Mcount_count_wait_xor_4_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(3),
      LI => C0_Mcount_count_wait_cy_4_rt_277,
      O => C0_Result(4)
    );
  C0_Mcount_count_wait_cy_4_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(3),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_4_rt_277,
      O => C0_Mcount_count_wait_cy(4)
    );
  C0_Mcount_count_wait_xor_3_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(2),
      LI => C0_Mcount_count_wait_cy_3_rt_278,
      O => C0_Result(3)
    );
  C0_Mcount_count_wait_cy_3_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(2),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_3_rt_278,
      O => C0_Mcount_count_wait_cy(3)
    );
  C0_Mcount_count_wait_xor_2_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(1),
      LI => C0_Mcount_count_wait_cy_2_rt_279,
      O => C0_Result(2)
    );
  C0_Mcount_count_wait_cy_2_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(1),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_2_rt_279,
      O => C0_Mcount_count_wait_cy(2)
    );
  C0_Mcount_count_wait_xor_1_Q : XORCY
    port map (
      CI => C0_Mcount_count_wait_cy(0),
      LI => C0_Mcount_count_wait_cy_1_rt_280,
      O => C0_Result(1)
    );
  C0_Mcount_count_wait_cy_1_Q : MUXCY
    port map (
      CI => C0_Mcount_count_wait_cy(0),
      DI => C0_count_wait(23),
      S => C0_Mcount_count_wait_cy_1_rt_280,
      O => C0_Mcount_count_wait_cy(1)
    );
  C0_Mcount_count_wait_xor_0_Q : XORCY
    port map (
      CI => C0_count_wait(23),
      LI => C0_Mcount_count_wait_lut(0),
      O => C0_Result(0)
    );
  C0_Mcount_count_wait_cy_0_Q : MUXCY
    port map (
      CI => C0_count_wait(23),
      DI => N1,
      S => C0_Mcount_count_wait_lut(0),
      O => C0_Mcount_count_wait_cy(0)
    );
  C0_send_data_7 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_7_dpot_346,
      Q => C0_send_data_7_Q
    );
  C0_send_data_6 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_6_dpot_345,
      Q => C0_send_data_6_Q
    );
  C0_send_data_5 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_5_dpot_344,
      Q => C0_send_data_5_Q
    );
  C0_send_data_3 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_3_dpot_343,
      Q => C0_send_data_3_Q
    );
  C0_send_data_1 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_1_dpot_342,
      Q => C0_send_data_1_Q
    );
  C0_send_data_0 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0297_inv2,
      D => C0_send_data_0_dpot_341,
      Q => C0_send_data_0_Q
    );
  C0_previousSTATE_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0460_inv,
      D => C0_STATE(1),
      R => rst_IBUF_0,
      Q => C0_previousSTATE(1)
    );
  C0_previousSTATE_0 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0460_inv,
      D => C0_n0452(0),
      R => rst_IBUF_0,
      Q => C0_previousSTATE(0)
    );
  C0_STATE_2 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_2_Q,
      R => rst_IBUF_0,
      Q => C0_STATE(2)
    );
  C0_STATE_1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_1_Q,
      R => rst_IBUF_0,
      Q => C0_STATE(1)
    );
  C0_STATE_0 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_0_Q,
      R => rst_IBUF_0,
      Q => C0_STATE(0)
    );
  C0_byte_count_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0357_inv,
      D => C0_n0343(2),
      R => rst_IBUF_0,
      Q => C0_byte_count(2)
    );
  C0_byte_count_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0357_inv,
      D => C0_n0343(1),
      R => rst_IBUF_0,
      Q => C0_byte_count(1)
    );
  C0_byte_count_0 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      CE => C0_n0357_inv,
      D => C0_n0343(0),
      R => rst_IBUF_0,
      Q => C0_byte_count(0)
    );
  C1_Mcount_spi_clk_count_xor_11_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(10),
      LI => C1_Mcount_spi_clk_count_xor_11_rt_293,
      O => C1_Result(11)
    );
  C1_Mcount_spi_clk_count_xor_10_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(9),
      LI => C1_Mcount_spi_clk_count_cy_10_rt_281,
      O => C1_Result(10)
    );
  C1_Mcount_spi_clk_count_cy_10_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(9),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_10_rt_281,
      O => C1_Mcount_spi_clk_count_cy(10)
    );
  C1_Mcount_spi_clk_count_xor_9_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(8),
      LI => C1_Mcount_spi_clk_count_cy_9_rt_282,
      O => C1_Result(9)
    );
  C1_Mcount_spi_clk_count_cy_9_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(8),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_9_rt_282,
      O => C1_Mcount_spi_clk_count_cy(9)
    );
  C1_Mcount_spi_clk_count_xor_8_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(7),
      LI => C1_Mcount_spi_clk_count_cy_8_rt_283,
      O => C1_Result(8)
    );
  C1_Mcount_spi_clk_count_cy_8_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(7),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_8_rt_283,
      O => C1_Mcount_spi_clk_count_cy(8)
    );
  C1_Mcount_spi_clk_count_xor_7_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(6),
      LI => C1_Mcount_spi_clk_count_cy_7_rt_284,
      O => C1_Result(7)
    );
  C1_Mcount_spi_clk_count_cy_7_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(6),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_7_rt_284,
      O => C1_Mcount_spi_clk_count_cy(7)
    );
  C1_Mcount_spi_clk_count_xor_6_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(5),
      LI => C1_Mcount_spi_clk_count_cy_6_rt_285,
      O => C1_Result(6)
    );
  C1_Mcount_spi_clk_count_cy_6_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(5),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_6_rt_285,
      O => C1_Mcount_spi_clk_count_cy(6)
    );
  C1_Mcount_spi_clk_count_xor_5_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(4),
      LI => C1_Mcount_spi_clk_count_cy_5_rt_286,
      O => C1_Result(5)
    );
  C1_Mcount_spi_clk_count_cy_5_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(4),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_5_rt_286,
      O => C1_Mcount_spi_clk_count_cy(5)
    );
  C1_Mcount_spi_clk_count_xor_4_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(3),
      LI => C1_Mcount_spi_clk_count_cy_4_rt_287,
      O => C1_Result(4)
    );
  C1_Mcount_spi_clk_count_cy_4_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(3),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_4_rt_287,
      O => C1_Mcount_spi_clk_count_cy(4)
    );
  C1_Mcount_spi_clk_count_xor_3_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(2),
      LI => C1_Mcount_spi_clk_count_cy_3_rt_288,
      O => C1_Result(3)
    );
  C1_Mcount_spi_clk_count_cy_3_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(2),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_3_rt_288,
      O => C1_Mcount_spi_clk_count_cy(3)
    );
  C1_Mcount_spi_clk_count_xor_2_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(1),
      LI => C1_Mcount_spi_clk_count_cy_2_rt_289,
      O => C1_Result(2)
    );
  C1_Mcount_spi_clk_count_cy_2_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(1),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_2_rt_289,
      O => C1_Mcount_spi_clk_count_cy(2)
    );
  C1_Mcount_spi_clk_count_xor_1_Q : XORCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(0),
      LI => C1_Mcount_spi_clk_count_cy_1_rt_290,
      O => C1_Result(1)
    );
  C1_Mcount_spi_clk_count_cy_1_Q : MUXCY
    port map (
      CI => C1_Mcount_spi_clk_count_cy(0),
      DI => C0_count_wait(23),
      S => C1_Mcount_spi_clk_count_cy_1_rt_290,
      O => C1_Mcount_spi_clk_count_cy(1)
    );
  C1_Mcount_spi_clk_count_xor_0_Q : XORCY
    port map (
      CI => C0_count_wait(23),
      LI => C1_Mcount_spi_clk_count_lut(0),
      O => C1_Result(0)
    );
  C1_Mcount_spi_clk_count_cy_0_Q : MUXCY
    port map (
      CI => C0_count_wait(23),
      DI => N1,
      S => C1_Mcount_spi_clk_count_lut(0),
      O => C1_Mcount_spi_clk_count_cy(0)
    );
  C1_RxTxSTATE_FSM_FFd2 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_FSM_FFd2_In,
      R => rst_IBUF_0,
      Q => C1_RxTxSTATE_FSM_FFd2_219
    );
  C1_RxTxSTATE_FSM_FFd1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_FSM_FFd1_In,
      R => rst_IBUF_0,
      Q => C1_RxTxSTATE_FSM_FFd1_220
    );
  C1_shift_register_7 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_7_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(7)
    );
  C1_shift_register_6 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_6_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(6)
    );
  C1_shift_register_5 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_5_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(5)
    );
  C1_shift_register_4 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_4_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(4)
    );
  C1_shift_register_3 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_3_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(3)
    );
  C1_shift_register_2 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_2_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(2)
    );
  C1_shift_register_1 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_1_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(1)
    );
  C1_shift_register_0 : FDR
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_0_Q,
      R => rst_IBUF_0,
      Q => C1_shift_register(0)
    );
  C1_rx_count_3 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C1_rst_inv,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_3_Q,
      Q => C1_rx_count(3)
    );
  C1_rx_count_2 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C1_rst_inv,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_2_Q,
      Q => C1_rx_count(2)
    );
  C1_rx_count_1 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C1_rst_inv,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_1_Q,
      Q => C1_rx_count(1)
    );
  C1_rx_count_0 : FDE
    port map (
      C => clk_BUFGP_1,
      CE => C1_rst_inv,
      D => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_0_Q,
      Q => C1_rx_count(0)
    );
  C0_Mmux_n034311 : LUT5
    generic map(
      INIT => X"40404054"
    )
    port map (
      I0 => C0_byte_count(0),
      I1 => C0_STATE(0),
      I2 => C0_STATE(1),
      I3 => C0_byte_count(1),
      I4 => C0_byte_count(2),
      O => C0_n0343(0)
    );
  C0_Mmux_n027351 : LUT5
    generic map(
      INIT => X"01115511"
    )
    port map (
      I0 => C0_byte_count(0),
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(2),
      I3 => C0_STATE(1),
      I4 => C0_STATE(0),
      O => C0_n0273_5_Q
    );
  C0_Mmux_n027361 : LUT5
    generic map(
      INIT => X"01000000"
    )
    port map (
      I0 => C0_byte_count(2),
      I1 => C0_byte_count(0),
      I2 => C0_byte_count(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(1),
      O => C0_n0273_6_Q
    );
  C0_Mmux_n027371 : LUT5
    generic map(
      INIT => X"010F0000"
    )
    port map (
      I0 => C0_byte_count(2),
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(0),
      I3 => C0_STATE(0),
      I4 => C0_STATE(1),
      O => C0_n0273_7_Q
    );
  C0_n0357_inv1 : LUT4
    generic map(
      INIT => X"5554"
    )
    port map (
      I0 => C0_STATE(2),
      I1 => start_IBUF_2,
      I2 => C0_STATE(0),
      I3 => C0_STATE(1),
      O => C0_n0357_inv
    );
  C0_n0460_inv1 : LUT3
    generic map(
      INIT => X"54"
    )
    port map (
      I0 => C0_STATE(2),
      I1 => C0_STATE(0),
      I2 => C0_STATE(1),
      O => C0_n0460_inv
    );
  C0_n0297_inv21 : LUT5
    generic map(
      INIT => X"7F030300"
    )
    port map (
      I0 => C0_byte_count(0),
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(2),
      I3 => C0_STATE(0),
      I4 => C0_STATE_1_2_347,
      O => C0_n0297_inv2
    );
  C0_Mmux_n034331 : LUT5
    generic map(
      INIT => X"08808080"
    )
    port map (
      I0 => C0_STATE(0),
      I1 => C0_STATE(1),
      I2 => C0_byte_count(2),
      I3 => C0_byte_count(0),
      I4 => C0_byte_count(1),
      O => C0_n0343(2)
    );
  C0_Mmux_n034321 : LUT5
    generic map(
      INIT => X"66020200"
    )
    port map (
      I0 => C0_byte_count(0),
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(2),
      I3 => C0_STATE(1),
      I4 => C0_STATE(0),
      O => C0_n0343(1)
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT5111 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => C0_STATE(0),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      O => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT511
    );
  C0_Mmux_STATE_2_X_7_o_Mux_127_o11 : LUT3
    generic map(
      INIT => X"CD"
    )
    port map (
      I0 => C0_STATE(0),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      O => C0_STATE_2_X_7_o_Mux_127_o
    );
  C0_Mmux_n045211 : LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => C0_STATE(0),
      I1 => C0_STATE(1),
      O => C0_n0452(0)
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1012 : LUT5
    generic map(
      INIT => X"F7B3A2B3"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd1_220,
      I1 => C0_begin_transmission_10,
      I2 => C0_slave_select_11,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011,
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT631 : LUT5
    generic map(
      INIT => X"00100000"
    )
    port map (
      I0 => C1_sclk_previous_14,
      I1 => C1_rx_count(3),
      I2 => C1_sclk_buffer_198,
      I3 => C1_RxTxSTATE_FSM_FFd1_220,
      I4 => C1_RxTxSTATE_FSM_FFd2_219,
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT10111 : LUT3
    generic map(
      INIT => X"FB"
    )
    port map (
      I0 => C1_sclk_previous_14,
      I1 => C1_sclk_buffer_198,
      I2 => C1_rx_count(3),
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1021 : LUT3
    generic map(
      INIT => X"1B"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd1_220,
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C0_slave_select_11,
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102
    );
  C1_RxTxSTATE_FSM_FFd1_In1 : LUT5
    generic map(
      INIT => X"888888F8"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd2_219,
      I1 => C1_rx_count(3),
      I2 => C1_RxTxSTATE_FSM_FFd1_220,
      I3 => C0_slave_select_11,
      I4 => C0_begin_transmission_10,
      O => C1_RxTxSTATE_FSM_FFd1_In
    );
  C0_n0484_inv_SW0 : LUT5
    generic map(
      INIT => X"FEFEFA00"
    )
    port map (
      I0 => C0_byte_count(2),
      I1 => C0_byte_count(0),
      I2 => C0_byte_count(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(1),
      O => N2
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT52 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => C0_previousSTATE(1),
      I1 => C1_end_transmission_12,
      I2 => C0_STATE(0),
      I3 => C0_STATE(1),
      O => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT51_223
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT53 : LUT6
    generic map(
      INIT => X"FFFFCCC0FFFFCC80"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT51_223,
      I4 => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT5,
      I5 => C0_count_wait_23_GND_7_o_equal_122_o,
      O => C0_STATE_2_X_7_o_wide_mux_130_OUT_1_Q
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT31 : LUT6
    generic map(
      INIT => X"C000FFCCFFCCAAAA"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C0_byte_count(2),
      I2 => C0_byte_count(0),
      I3 => C0_byte_count(1),
      I4 => C0_STATE(0),
      I5 => C0_STATE(1),
      O => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT3
    );
  C0_ss_count_11_PWR_7_o_equal_117_o_11_1 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C0_ss_count(7),
      I1 => C0_ss_count(6),
      I2 => C0_ss_count(9),
      I3 => C0_ss_count(8),
      I4 => C0_ss_count(11),
      I5 => C0_ss_count(10),
      O => C0_ss_count_11_PWR_7_o_equal_117_o_11_Q
    );
  C0_ss_count_11_PWR_7_o_equal_117_o_11_2 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C0_ss_count(1),
      I1 => C0_ss_count(0),
      I2 => C0_ss_count(3),
      I3 => C0_ss_count(2),
      I4 => C0_ss_count(5),
      I5 => C0_ss_count(4),
      O => C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227
    );
  C0_ss_count_11_PWR_7_o_equal_117_o_11_3 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_ss_count_11_PWR_7_o_equal_117_o_11_Q,
      I1 => C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227,
      O => C0_ss_count_11_PWR_7_o_equal_117_o
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_1 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C0_count_wait(11),
      I1 => C0_count_wait(10),
      I2 => C0_count_wait(13),
      I3 => C0_count_wait(12),
      I4 => C0_count_wait(15),
      I5 => C0_count_wait(14),
      O => C0_count_wait_23_GND_7_o_equal_122_o_23_Q
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_2 : LUT5
    generic map(
      INIT => X"80000000"
    )
    port map (
      I0 => C0_count_wait(19),
      I1 => C0_count_wait(18),
      I2 => C0_count_wait(22),
      I3 => C0_count_wait(21),
      I4 => C0_count_wait(20),
      O => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_3 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => C0_count_wait(1),
      I1 => C0_count_wait(0),
      I2 => C0_count_wait(3),
      I3 => C0_count_wait(2),
      O => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_4 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C0_count_wait(5),
      I1 => C0_count_wait(4),
      I2 => C0_count_wait(7),
      I3 => C0_count_wait(6),
      I4 => C0_count_wait(9),
      I5 => C0_count_wait(8),
      O => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C0_count_wait(16),
      I1 => C0_count_wait(17),
      I2 => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230,
      I3 => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229,
      I4 => C0_count_wait_23_GND_7_o_equal_122_o_23_Q,
      I5 => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231,
      O => C0_count_wait_23_GND_7_o_equal_122_o
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63 : LUT5
    generic map(
      INIT => X"EAAA4000"
    )
    port map (
      I0 => C1_rx_count(2),
      I1 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I2 => C1_rx_count(0),
      I3 => C1_rx_count(1),
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT61,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_2_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT81 : LUT5
    generic map(
      INIT => X"40000000"
    )
    port map (
      I0 => C1_sclk_previous_14,
      I1 => C1_sclk_buffer_198,
      I2 => C1_rx_count(0),
      I3 => C1_rx_count(1),
      I4 => C1_rx_count(2),
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT8
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT82 : LUT6
    generic map(
      INIT => X"B3BBA2AA80AA80AA"
    )
    port map (
      I0 => C1_rx_count(3),
      I1 => C1_RxTxSTATE_FSM_FFd1_220,
      I2 => C0_slave_select_11,
      I3 => C0_begin_transmission_10,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT8,
      I5 => C1_RxTxSTATE_FSM_FFd2_219,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_3_Q
    );
  C1_Reset_OR_DriverANDClockEnable11 : LUT6
    generic map(
      INIT => X"8000000000000000"
    )
    port map (
      I0 => C1_spi_clk_count(11),
      I1 => C1_spi_clk_count(0),
      I2 => C1_spi_clk_count(2),
      I3 => C1_spi_clk_count(1),
      I4 => C1_spi_clk_count(3),
      I5 => C1_spi_clk_count(4),
      O => C1_Reset_OR_DriverANDClockEnable11_234
    );
  C1_Reset_OR_DriverANDClockEnable12 : LUT5
    generic map(
      INIT => X"80000000"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd2_1_361,
      I1 => C1_spi_clk_count(10),
      I2 => C1_spi_clk_count(9),
      I3 => C1_spi_clk_count(6),
      I4 => C1_spi_clk_count(8),
      O => C1_Reset_OR_DriverANDClockEnable12_235
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT8_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_3_Q,
      O => N6
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT8 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(2),
      I1 => C1_shift_register(3),
      I2 => N6,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_3_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT6_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_1_Q,
      O => N8
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT6 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(1),
      I1 => C1_shift_register(2),
      I2 => N8,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_2_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT4 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(0),
      I1 => C1_shift_register(1),
      I2 => N8,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_1_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT2_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_0_Q,
      O => N12
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT2 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => JA_2_IBUF_3,
      I1 => C1_shift_register(0),
      I2 => N12,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_0_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT16_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_7_Q,
      O => N14
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT16 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(6),
      I1 => C1_shift_register(7),
      I2 => N14,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_7_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT14_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_6_Q,
      O => N16
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT14 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(5),
      I1 => C1_shift_register(6),
      I2 => N16,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_6_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT12_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C0_send_data_5_Q,
      O => N18
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT12 : LUT6
    generic map(
      INIT => X"FEEEFCCCFAAAF000"
    )
    port map (
      I0 => C1_shift_register(4),
      I1 => C1_shift_register(5),
      I2 => N18,
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT102,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101_153,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_5_Q
    );
  C1_n0119_inv2 : LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
    port map (
      I0 => C1_spi_clk_count(4),
      I1 => C1_spi_clk_count(3),
      I2 => C1_spi_clk_count(5),
      I3 => C1_spi_clk_count(6),
      I4 => C1_spi_clk_count(7),
      I5 => C1_spi_clk_count(8),
      O => C1_n0119_inv2_242
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_0
    );
  start_IBUF : IBUF
    port map (
      I => start,
      O => start_IBUF_2
    );
  JA_2_IBUF : IBUF
    port map (
      I => JA(2),
      O => JA_2_IBUF_3
    );
  JA_3_OBUF : OBUF
    port map (
      I => C1_sclk_previous_14,
      O => JA(3)
    );
  JA_1_OBUF : OBUF
    port map (
      I => C1_mosi_13,
      O => JA(1)
    );
  JA_0_OBUF : OBUF
    port map (
      I => C0_slave_select_11,
      O => JA(0)
    );
  C0_Mcount_ss_count_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(10),
      O => C0_Mcount_ss_count_cy_10_rt_250
    );
  C0_Mcount_ss_count_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(9),
      O => C0_Mcount_ss_count_cy_9_rt_251
    );
  C0_Mcount_ss_count_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(8),
      O => C0_Mcount_ss_count_cy_8_rt_252
    );
  C0_Mcount_ss_count_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(7),
      O => C0_Mcount_ss_count_cy_7_rt_253
    );
  C0_Mcount_ss_count_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(6),
      O => C0_Mcount_ss_count_cy_6_rt_254
    );
  C0_Mcount_ss_count_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(5),
      O => C0_Mcount_ss_count_cy_5_rt_255
    );
  C0_Mcount_ss_count_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(4),
      O => C0_Mcount_ss_count_cy_4_rt_256
    );
  C0_Mcount_ss_count_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(3),
      O => C0_Mcount_ss_count_cy_3_rt_257
    );
  C0_Mcount_ss_count_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(2),
      O => C0_Mcount_ss_count_cy_2_rt_258
    );
  C0_Mcount_ss_count_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(1),
      O => C0_Mcount_ss_count_cy_1_rt_259
    );
  C0_Mcount_count_wait_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(21),
      O => C0_Mcount_count_wait_cy_21_rt_260
    );
  C0_Mcount_count_wait_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(20),
      O => C0_Mcount_count_wait_cy_20_rt_261
    );
  C0_Mcount_count_wait_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(19),
      O => C0_Mcount_count_wait_cy_19_rt_262
    );
  C0_Mcount_count_wait_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(18),
      O => C0_Mcount_count_wait_cy_18_rt_263
    );
  C0_Mcount_count_wait_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(17),
      O => C0_Mcount_count_wait_cy_17_rt_264
    );
  C0_Mcount_count_wait_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(16),
      O => C0_Mcount_count_wait_cy_16_rt_265
    );
  C0_Mcount_count_wait_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(15),
      O => C0_Mcount_count_wait_cy_15_rt_266
    );
  C0_Mcount_count_wait_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(14),
      O => C0_Mcount_count_wait_cy_14_rt_267
    );
  C0_Mcount_count_wait_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(13),
      O => C0_Mcount_count_wait_cy_13_rt_268
    );
  C0_Mcount_count_wait_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(12),
      O => C0_Mcount_count_wait_cy_12_rt_269
    );
  C0_Mcount_count_wait_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(11),
      O => C0_Mcount_count_wait_cy_11_rt_270
    );
  C0_Mcount_count_wait_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(10),
      O => C0_Mcount_count_wait_cy_10_rt_271
    );
  C0_Mcount_count_wait_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(9),
      O => C0_Mcount_count_wait_cy_9_rt_272
    );
  C0_Mcount_count_wait_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(8),
      O => C0_Mcount_count_wait_cy_8_rt_273
    );
  C0_Mcount_count_wait_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(7),
      O => C0_Mcount_count_wait_cy_7_rt_274
    );
  C0_Mcount_count_wait_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(6),
      O => C0_Mcount_count_wait_cy_6_rt_275
    );
  C0_Mcount_count_wait_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(5),
      O => C0_Mcount_count_wait_cy_5_rt_276
    );
  C0_Mcount_count_wait_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(4),
      O => C0_Mcount_count_wait_cy_4_rt_277
    );
  C0_Mcount_count_wait_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(3),
      O => C0_Mcount_count_wait_cy_3_rt_278
    );
  C0_Mcount_count_wait_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(2),
      O => C0_Mcount_count_wait_cy_2_rt_279
    );
  C0_Mcount_count_wait_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(1),
      O => C0_Mcount_count_wait_cy_1_rt_280
    );
  C1_Mcount_spi_clk_count_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(10),
      O => C1_Mcount_spi_clk_count_cy_10_rt_281
    );
  C1_Mcount_spi_clk_count_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(9),
      O => C1_Mcount_spi_clk_count_cy_9_rt_282
    );
  C1_Mcount_spi_clk_count_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(8),
      O => C1_Mcount_spi_clk_count_cy_8_rt_283
    );
  C1_Mcount_spi_clk_count_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(7),
      O => C1_Mcount_spi_clk_count_cy_7_rt_284
    );
  C1_Mcount_spi_clk_count_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(6),
      O => C1_Mcount_spi_clk_count_cy_6_rt_285
    );
  C1_Mcount_spi_clk_count_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(5),
      O => C1_Mcount_spi_clk_count_cy_5_rt_286
    );
  C1_Mcount_spi_clk_count_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(4),
      O => C1_Mcount_spi_clk_count_cy_4_rt_287
    );
  C1_Mcount_spi_clk_count_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(3),
      O => C1_Mcount_spi_clk_count_cy_3_rt_288
    );
  C1_Mcount_spi_clk_count_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(2),
      O => C1_Mcount_spi_clk_count_cy_2_rt_289
    );
  C1_Mcount_spi_clk_count_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(1),
      O => C1_Mcount_spi_clk_count_cy_1_rt_290
    );
  C0_Mcount_ss_count_xor_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_ss_count(11),
      O => C0_Mcount_ss_count_xor_11_rt_291
    );
  C0_Mcount_count_wait_xor_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C0_count_wait(22),
      O => C0_Mcount_count_wait_xor_22_rt_292
    );
  C1_Mcount_spi_clk_count_xor_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => C1_spi_clk_count(11),
      O => C1_Mcount_spi_clk_count_xor_11_rt_293
    );
  C0_slave_select : FDS
    port map (
      C => clk_BUFGP_1,
      D => C0_slave_select_rstpot_294,
      S => rst_IBUF_0,
      Q => C0_slave_select_11
    );
  C1_mosi : FDS
    port map (
      C => clk_BUFGP_1,
      D => C1_mosi_rstpot_295,
      S => rst_IBUF_0,
      Q => C1_mosi_13
    );
  C1_sclk_previous : FDS
    port map (
      C => clk_BUFGP_1,
      D => C1_sclk_previous_rstpot_296,
      S => rst_IBUF_0,
      Q => C1_sclk_previous_14
    );
  C1_sclk_buffer : FDS
    port map (
      C => clk_BUFGP_1,
      D => C1_sclk_buffer_rstpot_297,
      S => rst_IBUF_0,
      Q => C1_sclk_buffer_198
    );
  C0_begin_transmission : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_begin_transmission_rstpot_298,
      Q => C0_begin_transmission_10
    );
  C1_end_transmission : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_end_transmission_rstpot_299,
      Q => C1_end_transmission_12
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT33 : LUT6
    generic map(
      INIT => X"000800F800F800F8"
    )
    port map (
      I0 => C0_previousSTATE(0),
      I1 => C1_end_transmission_12,
      I2 => C0_STATE(0),
      I3 => C0_STATE(1),
      I4 => C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227,
      I5 => C0_ss_count_11_PWR_7_o_equal_117_o_11_Q,
      O => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT32
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT61_SW0 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => C1_rx_count(1),
      I1 => C1_rx_count(0),
      O => N24
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT62 : LUT6
    generic map(
      INIT => X"DF8FDF8FDF8F8A8F"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd1_220,
      I1 => C0_slave_select_11,
      I2 => C0_begin_transmission_10,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      I4 => N24,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011,
      O => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT61
    );
  C1_Reset_OR_DriverANDClockEnable2 : LUT5
    generic map(
      INIT => X"EAAAAAAA"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => C1_spi_clk_count(5),
      I2 => C1_spi_clk_count(7),
      I3 => C1_Reset_OR_DriverANDClockEnable12_235,
      I4 => C1_Reset_OR_DriverANDClockEnable11_234,
      O => C1_Reset_OR_DriverANDClockEnable
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW0 : LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C0_STATE(0),
      I2 => C0_STATE(1),
      O => N26
    );
  C0_slave_select_rstpot : LUT6
    generic map(
      INIT => X"F0F0AAB8AAB8AAB8"
    )
    port map (
      I0 => C0_slave_select_11,
      I1 => C0_STATE(2),
      I2 => C0_STATE_2_X_7_o_Mux_127_o,
      I3 => N2,
      I4 => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT511,
      I5 => C0_ss_count_11_PWR_7_o_equal_117_o,
      O => C0_slave_select_rstpot_294
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW2 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => C0_STATE_2_1_339,
      I1 => C0_STATE_1_1_338,
      I2 => C0_count_wait(17),
      I3 => C0_count_wait(16),
      O => N29
    );
  C0_Reset_OR_DriverANDClockEnable361 : LUT6
    generic map(
      INIT => X"EAAAAAAAAAAAAAAA"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => N29,
      I2 => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230,
      I3 => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229,
      I4 => C0_count_wait_23_GND_7_o_equal_122_o_23_Q,
      I5 => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231,
      O => C0_Reset_OR_DriverANDClockEnable
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT32_SW0 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230,
      I1 => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229,
      I2 => C0_count_wait_23_GND_7_o_equal_122_o_23_Q,
      I3 => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231,
      O => N31
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT34 : LUT6
    generic map(
      INIT => X"EEEE44EEEEEE4E4E"
    )
    port map (
      I0 => C0_STATE(2),
      I1 => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT3,
      I2 => N26,
      I3 => N27,
      I4 => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT32,
      I5 => N31,
      O => C0_STATE_2_X_7_o_wide_mux_130_OUT_0_Q
    );
  C0_count_wait_0 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_0_rstpot_305,
      Q => C0_count_wait(0)
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT4_SW0_SW0 : LUT4
    generic map(
      INIT => X"DF8F"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd1_220,
      I1 => C0_slave_select_11,
      I2 => C0_begin_transmission_10,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      O => N33
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT4_SW0_SW1 : LUT4
    generic map(
      INIT => X"A2B3"
    )
    port map (
      I0 => C1_RxTxSTATE_FSM_FFd1_220,
      I1 => C0_begin_transmission_10,
      I2 => C0_slave_select_11,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      O => N34
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT4 : LUT6
    generic map(
      INIT => X"D8F8D85888A88808"
    )
    port map (
      I0 => C1_rx_count(1),
      I1 => N33,
      I2 => C1_rx_count(0),
      I3 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011,
      I4 => N34,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_1_Q
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW3 : LUT5
    generic map(
      INIT => X"55000C0F"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C1_end_transmission_12,
      I2 => C0_STATE(0),
      I3 => C0_STATE(2),
      I4 => C0_STATE(1),
      O => N36
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT61 : LUT6
    generic map(
      INIT => X"00007FFF8000FFFF"
    )
    port map (
      I0 => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230,
      I1 => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229,
      I2 => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231,
      I3 => C0_count_wait_23_GND_7_o_equal_122_o_23_Q,
      I4 => N36,
      I5 => N37,
      O => C0_STATE_2_X_7_o_wide_mux_130_OUT_2_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT101 : LUT6
    generic map(
      INIT => X"FFFFA280A280A280"
    )
    port map (
      I0 => C1_shift_register(4),
      I1 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011,
      I2 => N33,
      I3 => N34,
      I4 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      I5 => C1_shift_register(3),
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_21_OUT_4_Q
    );
  C0_count_wait_0_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(0),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(0),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_0_rstpot_305
    );
  C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT51 : LUT5
    generic map(
      INIT => X"40000000"
    )
    port map (
      I0 => C0_STATE(1),
      I1 => C0_STATE(2),
      I2 => C0_STATE(0),
      I3 => C0_ss_count_11_PWR_7_o_equal_117_o_11_Q,
      I4 => C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227,
      O => C0_Mmux_STATE_2_X_7_o_wide_mux_130_OUT5
    );
  C0_Reset_OR_DriverANDClockEnable241 : LUT6
    generic map(
      INIT => X"AAEAAAAAAAAAAAAA"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => C0_STATE(2),
      I2 => C0_STATE(0),
      I3 => C0_STATE_1_2_347,
      I4 => C0_ss_count_11_PWR_7_o_equal_117_o_11_1_227,
      I5 => C0_ss_count_11_PWR_7_o_equal_117_o_11_Q,
      O => C0_Reset_OR_DriverANDClockEnable24
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW1 : MUXF7
    port map (
      I0 => N45,
      I1 => N46,
      S => C0_count_wait(17),
      O => N27
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW1_F : LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C0_STATE(0),
      I2 => C0_STATE(1),
      O => N45
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW1_G : LUT6
    generic map(
      INIT => X"A2F7F7F7FFFFFFFF"
    )
    port map (
      I0 => C0_count_wait(16),
      I1 => C0_previousSTATE(1),
      I2 => C0_previousSTATE(0),
      I3 => start_IBUF_2,
      I4 => C0_STATE(0),
      I5 => C0_STATE(1),
      O => N46
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW4 : MUXF7
    port map (
      I0 => N47,
      I1 => N48,
      S => C0_count_wait(17),
      O => N37
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW4_F : LUT5
    generic map(
      INIT => X"55000C0F"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C1_end_transmission_12,
      I2 => C0_STATE(0),
      I3 => C0_STATE(2),
      I4 => C0_STATE(1),
      O => N47
    );
  C0_count_wait_23_GND_7_o_equal_122_o_23_5_SW4_G : LUT6
    generic map(
      INIT => X"F5F5000000CC00FF"
    )
    port map (
      I0 => start_IBUF_2,
      I1 => C1_end_transmission_12,
      I2 => C0_count_wait(16),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_STATE(1),
      O => N48
    );
  C1_end_transmission_rstpot : LUT4
    generic map(
      INIT => X"ACA8"
    )
    port map (
      I0 => C1_end_transmission_12,
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => rst_IBUF_0,
      I3 => C1_rx_count(3),
      O => C1_end_transmission_rstpot_299
    );
  C1_mosi_rstpot_SW0 : LUT4
    generic map(
      INIT => X"AEA2"
    )
    port map (
      I0 => C1_mosi_13,
      I1 => C1_sclk_previous_14,
      I2 => C1_sclk_buffer_198,
      I3 => C1_shift_register(7),
      O => N49
    );
  C1_mosi_rstpot : LUT6
    generic map(
      INIT => X"EFEEECEEABAAA8AA"
    )
    port map (
      I0 => C1_mosi_13,
      I1 => C1_RxTxSTATE_FSM_FFd1_220,
      I2 => C1_rx_count(3),
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      I4 => N49,
      I5 => C0_slave_select_11,
      O => C1_mosi_rstpot_295
    );
  C1_sclk_previous_rstpot_SW0 : LUT6
    generic map(
      INIT => X"EAAAAAAA2AAAAAAA"
    )
    port map (
      I0 => C1_sclk_buffer_198,
      I1 => C1_spi_clk_count(10),
      I2 => C1_spi_clk_count(11),
      I3 => C1_spi_clk_count(2),
      I4 => C1_spi_clk_count(9),
      I5 => C1_sclk_previous_14,
      O => N51
    );
  C1_sclk_previous_rstpot : LUT6
    generic map(
      INIT => X"BAAA8AAAFFFFFFFF"
    )
    port map (
      I0 => C1_sclk_buffer_198,
      I1 => C1_n0119_inv2_242,
      I2 => C1_spi_clk_count(1),
      I3 => C1_spi_clk_count(0),
      I4 => N51,
      I5 => C1_RxTxSTATE_FSM_FFd2_219,
      O => C1_sclk_previous_rstpot_296
    );
  C1_sclk_buffer_rstpot : LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
    port map (
      I0 => C1_sclk_buffer_198,
      I1 => C1_Reset_OR_DriverANDClockEnable11_234,
      I2 => C1_spi_clk_count(7),
      I3 => C1_spi_clk_count(5),
      I4 => C1_Reset_OR_DriverANDClockEnable12_235,
      O => C1_sclk_buffer_rstpot_297
    );
  C0_begin_transmission_rstpot : LUT4
    generic map(
      INIT => X"C5C4"
    )
    port map (
      I0 => C0_STATE(2),
      I1 => C0_begin_transmission_10,
      I2 => rst_IBUF_0,
      I3 => C0_n0297_inv2,
      O => C0_begin_transmission_rstpot_298
    );
  C1_RxTxSTATE_FSM_FFd2_In1 : LUT5
    generic map(
      INIT => X"082AFF2A"
    )
    port map (
      I0 => C0_begin_transmission_10,
      I1 => C1_RxTxSTATE_FSM_FFd1_220,
      I2 => C0_slave_select_11,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      I4 => C1_rx_count(3),
      O => C1_RxTxSTATE_FSM_FFd2_In
    );
  C0_Mmux_n027341 : LUT5
    generic map(
      INIT => X"0100FCFC"
    )
    port map (
      I0 => C0_byte_count(2),
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(0),
      I3 => C0_STATE(0),
      I4 => C0_STATE(1),
      O => C0_n0273_3_Q
    );
  C0_count_wait_22 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_22_rstpot_316,
      Q => C0_count_wait(22)
    );
  C0_count_wait_21 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_21_rstpot_317,
      Q => C0_count_wait(21)
    );
  C0_count_wait_20 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_20_rstpot_318,
      Q => C0_count_wait(20)
    );
  C0_count_wait_19 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_19_rstpot_319,
      Q => C0_count_wait(19)
    );
  C0_count_wait_18 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_18_rstpot_320,
      Q => C0_count_wait(18)
    );
  C0_count_wait_17 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_17_rstpot_321,
      Q => C0_count_wait(17)
    );
  C0_count_wait_16 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_16_rstpot_322,
      Q => C0_count_wait(16)
    );
  C0_count_wait_15 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_15_rstpot_323,
      Q => C0_count_wait(15)
    );
  C0_count_wait_14 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_14_rstpot_324,
      Q => C0_count_wait(14)
    );
  C0_count_wait_13 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_13_rstpot_325,
      Q => C0_count_wait(13)
    );
  C0_count_wait_12 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_12_rstpot_326,
      Q => C0_count_wait(12)
    );
  C0_count_wait_11 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_11_rstpot_327,
      Q => C0_count_wait(11)
    );
  C0_count_wait_10 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_10_rstpot_328,
      Q => C0_count_wait(10)
    );
  C0_count_wait_9 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_9_rstpot_329,
      Q => C0_count_wait(9)
    );
  C0_count_wait_8 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_8_rstpot_330,
      Q => C0_count_wait(8)
    );
  C0_count_wait_7 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_7_rstpot_331,
      Q => C0_count_wait(7)
    );
  C0_count_wait_6 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_6_rstpot_332,
      Q => C0_count_wait(6)
    );
  C0_count_wait_5 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_5_rstpot_333,
      Q => C0_count_wait(5)
    );
  C0_count_wait_4 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_4_rstpot_334,
      Q => C0_count_wait(4)
    );
  C0_count_wait_3 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_3_rstpot_335,
      Q => C0_count_wait(3)
    );
  C0_count_wait_2 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_2_rstpot_336,
      Q => C0_count_wait(2)
    );
  C0_count_wait_1 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_count_wait_1_rstpot_337,
      Q => C0_count_wait(1)
    );
  C0_STATE_1_1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_1_Q,
      R => rst_IBUF_0,
      Q => C0_STATE_1_1_338
    );
  C0_count_wait_22_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(22),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(22),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_22_rstpot_316
    );
  C0_count_wait_21_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(21),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(21),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_21_rstpot_317
    );
  C0_count_wait_20_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(20),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(20),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_20_rstpot_318
    );
  C0_count_wait_19_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(19),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(19),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_19_rstpot_319
    );
  C0_count_wait_18_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(18),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(18),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_18_rstpot_320
    );
  C0_count_wait_17_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(17),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(17),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_17_rstpot_321
    );
  C0_count_wait_16_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(16),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(16),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_16_rstpot_322
    );
  C0_count_wait_15_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(15),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(15),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_15_rstpot_323
    );
  C0_count_wait_14_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(14),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(14),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_14_rstpot_324
    );
  C0_count_wait_13_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(13),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(13),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_13_rstpot_325
    );
  C0_count_wait_12_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(12),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(12),
      I4 => C0_Reset_OR_DriverANDClockEnable,
      O => C0_count_wait_12_rstpot_326
    );
  C0_count_wait_11_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(11),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(11),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_11_rstpot_327
    );
  C0_count_wait_10_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(10),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(10),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_10_rstpot_328
    );
  C0_count_wait_9_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(9),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(9),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_9_rstpot_329
    );
  C0_count_wait_8_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(8),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(8),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_8_rstpot_330
    );
  C0_count_wait_7_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(7),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(7),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_7_rstpot_331
    );
  C0_count_wait_6_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(6),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(6),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_6_rstpot_332
    );
  C0_count_wait_5_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(5),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(5),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_5_rstpot_333
    );
  C0_count_wait_4_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(4),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(4),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_4_rstpot_334
    );
  C0_count_wait_3_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(3),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(3),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_3_rstpot_335
    );
  C0_count_wait_2_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(2),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(2),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_2_rstpot_336
    );
  C0_count_wait_1_rstpot : LUT5
    generic map(
      INIT => X"0000EA2A"
    )
    port map (
      I0 => C0_count_wait(1),
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_Result(1),
      I4 => C0_Reset_OR_DriverANDClockEnable361_360,
      O => C0_count_wait_1_rstpot_337
    );
  C0_STATE_2_1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_2_Q,
      R => rst_IBUF_0,
      Q => C0_STATE_2_1_339
    );
  C0_n0297_inv1_rstpot : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => C0_STATE(2),
      O => C0_n0297_inv1_rstpot_340
    );
  C0_send_data_5_dpot : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => C0_n0297_inv1_rstpot_340,
      I1 => C0_send_data_5_Q,
      I2 => C0_n0273_5_Q,
      O => C0_send_data_5_dpot_344
    );
  C0_STATE_1_2 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C0_STATE_2_X_7_o_wide_mux_130_OUT_1_Q,
      R => rst_IBUF_0,
      Q => C0_STATE_1_2_347
    );
  C1_spi_clk_count_11_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(11),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(11),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_11_rstpot_348
    );
  C1_spi_clk_count_11 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_11_rstpot_348,
      Q => C1_spi_clk_count(11)
    );
  C1_spi_clk_count_10_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(10),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(10),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_10_rstpot_349
    );
  C1_spi_clk_count_10 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_10_rstpot_349,
      Q => C1_spi_clk_count(10)
    );
  C1_spi_clk_count_9_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(9),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(9),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_9_rstpot_350
    );
  C1_spi_clk_count_9 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_9_rstpot_350,
      Q => C1_spi_clk_count(9)
    );
  C1_spi_clk_count_8_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(8),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(8),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_8_rstpot_351
    );
  C1_spi_clk_count_8 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_8_rstpot_351,
      Q => C1_spi_clk_count(8)
    );
  C1_spi_clk_count_7_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(7),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(7),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_7_rstpot_352
    );
  C1_spi_clk_count_7 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_7_rstpot_352,
      Q => C1_spi_clk_count(7)
    );
  C1_spi_clk_count_6_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(6),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(6),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_6_rstpot_353
    );
  C1_spi_clk_count_6 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_6_rstpot_353,
      Q => C1_spi_clk_count(6)
    );
  C1_spi_clk_count_5_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(5),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(5),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_5_rstpot_354
    );
  C1_spi_clk_count_5 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_5_rstpot_354,
      Q => C1_spi_clk_count(5)
    );
  C1_spi_clk_count_4_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(4),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(4),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_4_rstpot_355
    );
  C1_spi_clk_count_4 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_4_rstpot_355,
      Q => C1_spi_clk_count(4)
    );
  C1_spi_clk_count_3_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(3),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(3),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_3_rstpot_356
    );
  C1_spi_clk_count_3 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_3_rstpot_356,
      Q => C1_spi_clk_count(3)
    );
  C1_spi_clk_count_2_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(2),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(2),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_2_rstpot_357
    );
  C1_spi_clk_count_2 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_2_rstpot_357,
      Q => C1_spi_clk_count(2)
    );
  C1_spi_clk_count_1_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(1),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(1),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_1_rstpot_358
    );
  C1_spi_clk_count_1 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_1_rstpot_358,
      Q => C1_spi_clk_count(1)
    );
  C1_spi_clk_count_0_rstpot : LUT4
    generic map(
      INIT => X"00E2"
    )
    port map (
      I0 => C1_spi_clk_count(0),
      I1 => C1_RxTxSTATE_FSM_FFd2_219,
      I2 => C1_Result(0),
      I3 => C1_Reset_OR_DriverANDClockEnable,
      O => C1_spi_clk_count_0_rstpot_359
    );
  C1_spi_clk_count_0 : FD
    port map (
      C => clk_BUFGP_1,
      D => C1_spi_clk_count_0_rstpot_359,
      Q => C1_spi_clk_count(0)
    );
  C0_Reset_OR_DriverANDClockEnable361_1 : LUT6
    generic map(
      INIT => X"EAAAAAAAAAAAAAAA"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => N29,
      I2 => C0_count_wait_23_GND_7_o_equal_122_o_23_2_230,
      I3 => C0_count_wait_23_GND_7_o_equal_122_o_23_1_229,
      I4 => C0_count_wait_23_GND_7_o_equal_122_o_23_Q,
      I5 => C0_count_wait_23_GND_7_o_equal_122_o_23_3_231,
      O => C0_Reset_OR_DriverANDClockEnable361_360
    );
  C0_send_data_1_dpot : LUT6
    generic map(
      INIT => X"000FFCFCAAAAAAAA"
    )
    port map (
      I0 => C0_send_data_1_Q,
      I1 => C0_byte_count(1),
      I2 => C0_byte_count(0),
      I3 => C0_STATE(0),
      I4 => C0_STATE(1),
      I5 => C0_n0297_inv1_rstpot_340,
      O => C0_send_data_1_dpot_342
    );
  C1_RxTxSTATE_FSM_FFd2_1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_1,
      D => C1_RxTxSTATE_FSM_FFd2_In,
      R => rst_IBUF_0,
      Q => C1_RxTxSTATE_FSM_FFd2_1_361
    );
  C0_send_data_3_dpot : LUT4
    generic map(
      INIT => X"ABA8"
    )
    port map (
      I0 => C0_send_data_3_Q,
      I1 => rst_IBUF_0,
      I2 => C0_STATE(2),
      I3 => C0_n0273_3_Q,
      O => C0_send_data_3_dpot_343
    );
  C0_send_data_6_dpot : LUT4
    generic map(
      INIT => X"ABA8"
    )
    port map (
      I0 => C0_send_data_6_Q,
      I1 => rst_IBUF_0,
      I2 => C0_STATE(2),
      I3 => C0_n0273_6_Q,
      O => C0_send_data_6_dpot_345
    );
  C0_send_data_7_dpot : LUT4
    generic map(
      INIT => X"ABA8"
    )
    port map (
      I0 => C0_send_data_7_Q,
      I1 => rst_IBUF_0,
      I2 => C0_STATE(2),
      I3 => C0_n0273_7_Q,
      O => C0_send_data_7_dpot_346
    );
  C0_ss_count_11 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_11_rstpot_362,
      Q => C0_ss_count(11)
    );
  C0_ss_count_10 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_10_rstpot_363,
      Q => C0_ss_count(10)
    );
  C0_ss_count_9 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_9_rstpot_364,
      Q => C0_ss_count(9)
    );
  C0_ss_count_8 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_8_rstpot_365,
      Q => C0_ss_count(8)
    );
  C0_ss_count_7 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_7_rstpot_366,
      Q => C0_ss_count(7)
    );
  C0_ss_count_6 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_6_rstpot_367,
      Q => C0_ss_count(6)
    );
  C0_ss_count_5 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_5_rstpot_368,
      Q => C0_ss_count(5)
    );
  C0_ss_count_4 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_4_rstpot_369,
      Q => C0_ss_count(4)
    );
  C0_ss_count_3 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_3_rstpot_370,
      Q => C0_ss_count(3)
    );
  C0_ss_count_2 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_2_rstpot_371,
      Q => C0_ss_count(2)
    );
  C0_ss_count_1 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_1_rstpot_372,
      Q => C0_ss_count(1)
    );
  C0_ss_count_0 : FD
    port map (
      C => clk_BUFGP_1,
      D => C0_ss_count_0_rstpot_373,
      Q => C0_ss_count(0)
    );
  C0_send_data_0_dpot : LUT6
    generic map(
      INIT => X"EFEFEFEE01010100"
    )
    port map (
      I0 => rst_IBUF_0,
      I1 => C0_STATE(2),
      I2 => C0_STATE(1),
      I3 => C0_byte_count(0),
      I4 => C0_byte_count(1),
      I5 => C0_send_data_0_Q,
      O => C0_send_data_0_dpot_341
    );
  C0_ss_count_11_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(11),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_11_1,
      O => C0_ss_count_11_rstpot_362
    );
  C0_ss_count_10_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(10),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_10_1,
      O => C0_ss_count_10_rstpot_363
    );
  C0_ss_count_9_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(9),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_9_1,
      O => C0_ss_count_9_rstpot_364
    );
  C0_ss_count_8_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(8),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_8_1,
      O => C0_ss_count_8_rstpot_365
    );
  C0_ss_count_7_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(7),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_7_1,
      O => C0_ss_count_7_rstpot_366
    );
  C0_ss_count_6_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(6),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_6_1,
      O => C0_ss_count_6_rstpot_367
    );
  C0_ss_count_5_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(5),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_5_1,
      O => C0_ss_count_5_rstpot_368
    );
  C0_ss_count_4_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(4),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_4_1,
      O => C0_ss_count_4_rstpot_369
    );
  C0_ss_count_3_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(3),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_3_1,
      O => C0_ss_count_3_rstpot_370
    );
  C0_ss_count_2_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(2),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_2_1,
      O => C0_ss_count_2_rstpot_371
    );
  C0_ss_count_1_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(1),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_1_1,
      O => C0_ss_count_1_rstpot_372
    );
  C0_ss_count_0_rstpot : LUT6
    generic map(
      INIT => X"4544444440444444"
    )
    port map (
      I0 => C0_Reset_OR_DriverANDClockEnable24,
      I1 => C0_ss_count(0),
      I2 => C0_STATE(1),
      I3 => C0_STATE(0),
      I4 => C0_STATE(2),
      I5 => C0_Result_0_1,
      O => C0_ss_count_0_rstpot_373
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_1
    );
  C0_Mcount_ss_count_lut_0_INV_0 : INV
    port map (
      I => C0_ss_count(0),
      O => C0_Mcount_ss_count_lut(0)
    );
  C0_Mcount_count_wait_lut_0_INV_0 : INV
    port map (
      I => C0_count_wait(0),
      O => C0_Mcount_count_wait_lut(0)
    );
  C1_Mcount_spi_clk_count_lut_0_INV_0 : INV
    port map (
      I => C1_spi_clk_count(0),
      O => C1_Mcount_spi_clk_count_lut(0)
    );
  C1_rst_inv1_INV_0 : INV
    port map (
      I => rst_IBUF_0,
      O => C1_rst_inv
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT21 : MUXF7
    port map (
      I0 => N53,
      I1 => N54,
      S => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_21_OUT1011,
      O => C1_RxTxSTATE_1_X_8_o_wide_mux_20_OUT_0_Q
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT21_F : LUT6
    generic map(
      INIT => X"D5DDD5FF808880AA"
    )
    port map (
      I0 => C1_rx_count(0),
      I1 => C1_RxTxSTATE_FSM_FFd1_220,
      I2 => C0_slave_select_11,
      I3 => C0_begin_transmission_10,
      I4 => C1_RxTxSTATE_FSM_FFd2_219,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      O => N53
    );
  C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT21_G : LUT6
    generic map(
      INIT => X"FFF77F77AAA22A22"
    )
    port map (
      I0 => C1_rx_count(0),
      I1 => C0_begin_transmission_10,
      I2 => C1_RxTxSTATE_FSM_FFd1_220,
      I3 => C1_RxTxSTATE_FSM_FFd2_219,
      I4 => C0_slave_select_11,
      I5 => C1_Mmux_RxTxSTATE_1_X_8_o_wide_mux_20_OUT63_151,
      O => N54
    );

end Structure;

