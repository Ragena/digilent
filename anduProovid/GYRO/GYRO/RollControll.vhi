
-- VHDL Instantiation Created from source file RollControll.vhd -- 18:00:28 04/19/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT RollControll
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		duty_in : IN std_logic_vector(0 to 18);
		Axis_data : IN std_logic_vector(15 downto 0);          
		LeftPWM : OUT std_logic;
		RightPWM : OUT std_logic
		);
	END COMPONENT;

	Inst_RollControll: RollControll PORT MAP(
		clk => ,
		rst => ,
		duty_in => ,
		Axis_data => ,
		LeftPWM => ,
		RightPWM => 
	);


