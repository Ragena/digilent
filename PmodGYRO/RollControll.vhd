----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:53:08 04/19/2013 
-- Design Name: 
-- Module Name:    AxisControll - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--use DutyCycleDef.all;

entity AxisControll is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           duty_in : in  integer range 100000 to 300000;
           Axis_data : in  STD_LOGIC_VECTOR (15 downto 0);
           LeftPWM : out  STD_LOGIC;
           RightPWM : out  STD_LOGIC);
end AxisControll;

architecture Behavioral of AxisControll is

COMPONENT PulseWidth
	PORT(
		clk : IN std_logic;
		RST : IN std_logic;
		Duty_in : IN integer range 100000 to 300000;
		btnl : IN std_logic;
		btnr : IN std_logic;          
		clk50Hz : OUT std_logic;
		PWM : OUT std_logic;
		Duty_out : OUT std_logic
		);
	END COMPONENT;

constant duty_min : integer := 100000;
constant duty_max : integer := 300000;
constant duty_step : integer := 100;
constant negative_begin : std_logic_vector := X"8000";


signal d_duty : integer;
signal leftDuty, rightDuty : integer range duty_min to duty_max;

begin

d_duty <= duty_step * conv_integer(Axis_data);
leftDuty <= duty_in - duty_step;
rightDuty <= duty_in + duty_step;

	leftMotor: PulseWidth PORT MAP(
		clk => clk,
		clk50Hz => open,
		PWM => leftPWM,
		RST => rst,
		Duty_in => leftDuty,
		btnl => '0',
		Duty_out => open,
		btnr => '0'
	);
	
	rightMotor: PulseWidth PORT MAP(
		clk => clk,
		clk50Hz => open,
		PWM => rightPWM,
		RST => rst,
		Duty_in => rightDuty,
		btnl => '0',
		Duty_out => open,
		btnr => '0'
	);

end Behavioral;

