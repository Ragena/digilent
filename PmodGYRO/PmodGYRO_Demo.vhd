--////////////////////////////////////////////////////////////////////////////////////////
-- Company: Digilent Inc.
-- Engineer: Andrew Skreen
-- 
-- Create Date:    08/16/2011
-- Module Name:    PmodGYRO_Demo
-- Project Name: 	 PmodGYRO_Demo
-- Target Devices: Nexys3
-- Tool versions:  ISE 14.1
-- Description: This demo configures the PmodGYRO to output data at a rate of 100 Hz
-- 				 with 8.75 mdps/digit at 250 dps maximum.  SPI mode 3 is used for data
--					 communication with the PmodGYRO.
--
--					 Switches SW3 and SW2 are used to select temperature or axis data that is 
--					 to be displayed on the seven segment display (SSD).  For details about
--					 selecting data see below.
--
--						SW3  |  SW2  |  Display Data
--						----------------------------
--						 0   |   0   |  X axis data
--						 0	  |   1   |  Y axis data
--						 1   |   0   |  Z axis data
--						 1   |   1   |  Temperature
--
--  Inputs:
--		clk 						Base system clock of 100 MHz
--		sw[0]						Reset signal
--		sw[1] 					Start tied to external user input
--		sw[2]						Data select bit 0
--		sw[3]						Data select bit 1
--		sw[4]						Select hex display or decimal display
--		JA[2] 					Master in slave out (MISO)
--		
--  Outputs:
--		JA[0]						Slave select (SS)
--		JA[1]						Master out slave in (MOSI)
--		JA[3]						Serial clock (SCLK)
--		seg						Cathodes on SSD
--		dp							Decimal on SSD
--		an							Anodes on SSD
--
-- Revision History: 
-- 						Revision 0.01 - File Created (Andrew Skreen)
--							Revision 1.00 - Added Comments and Converted to Verilog (Josh Sackos)
--////////////////////////////////////////////////////////////////////////////////////////
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

--use DutyCycleDef.all;

-- ==============================================================================
-- 										  Define Module
-- ==============================================================================
entity GYRO is
    Port ( rst : in  STD_LOGIC;
			  clk : in STD_LOGIC;
			  --start : in std_logic;
			  go : in std_logic;
			  --duty_in : in integer;
           ---GYRO_x_axis, GYRO_y_axis, GYRO_z_axis : out std_logic_vector (15 downto 0);
           JA : inout  STD_LOGIC_VECTOR (3 downto 0);
			  JD : out STD_LOGIC_VECTOR (3 downto 0));
end GYRO;

architecture Behavioral of GYRO is

-- ==============================================================================
-- 									Component Declarations
-- ==============================================================================
			
			component master_interface
				port ( begin_transmission : out std_logic;
						 recieved_data : in std_logic_vector(7 downto 0);
						 end_transmission : in std_logic;
						 clk : in std_logic;
						 rst : in std_logic;
						 start : in std_logic;
						 slave_select : out std_logic;
						 send_data : out std_logic_vector(7 downto 0);
						 temp_data : out std_logic_vector(7 downto 0);
						 x_axis_data : out std_logic_vector(15 downto 0);
						 y_axis_data : out std_logic_vector(15 downto 0);
						 z_axis_data : out std_logic_vector(15 downto 0));
			end component;


			component spi_interface
				port ( begin_transmission : in std_logic;
						 slave_select : in std_logic;
						 send_data : in std_logic_vector(7 downto 0);
						 miso : in std_logic;
						 clk : in std_logic;
						 rst : in std_logic;
						 recieved_data : out std_logic_vector( 7 downto 0);
						 end_transmission : out std_logic;
						 mosi : out std_logic;
						 sclk : out std_logic);
			end component;

COMPONENT AxisControll
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		duty_in : IN integer range 100000 to 300000;
		Axis_data : IN std_logic_vector(15 downto 0);          
		LeftPWM : OUT std_logic;
		RightPWM : OUT std_logic
		);
	END COMPONENT;

-- ==============================================================================
-- 								  Signals, Constants, Types
-- ==============================================================================

			signal begin_transmission : std_logic;
			signal end_transmission : std_logic;
			signal send_data : std_logic_vector(7 downto 0);
			signal recieved_data : std_logic_vector(7 downto 0);
			signal temp_data : std_logic_vector(7 downto 0);
			signal slave_select : std_logic;
			signal GyroOutX, GyroOutY, GyroOutZ : std_logic_vector (15 downto 0) := (others => '0');
			signal duty_in : integer;
			--signal hajime : std_logic := '0';

-- ==============================================================================
-- 										  Implementation
-- ==============================================================================
begin

duty_in <= 110000 when go = '1' else
	100000;

	Roll: AxisControll PORT MAP(
		clk => clk,
		rst => rst,
		duty_in => duty_in,
		Axis_data => GyroOutX,
		LeftPWM => JD(2),
		RightPWM => JD(3)
	);

	Pitch: AxisControll PORT MAP(
		clk => clk,
		rst => rst,
		duty_in => duty_in,
		Axis_data => GyroOutY,
		LeftPWM => JD(1), --Rear*
		RightPWM => JD(0)--Front*
	);
	
			----------------------------------------
			--		Serial Port Interface Controller
			----------------------------------------
			C0 : master_interface port map (
						begin_transmission => begin_transmission,
						end_transmission => end_transmission,
						send_data => send_data,
						recieved_data => recieved_data,
						clk => clk,
						rst => rst,
						slave_select => slave_select,
						start => go,
						temp_data => temp_data,
						x_axis_data => GyroOutX,
						y_axis_data => GyroOutY,
						z_axis_data => GyroOutZ
			);

			----------------------------------------
			--		    Serial Port Interface
			----------------------------------------
			C1 : spi_interface port map (
						begin_transmission => begin_transmission,
						slave_select => slave_select,
						send_data => send_data,
						recieved_data => recieved_data,
						miso => JA(2),
						clk => clk,
						rst => rst,
						end_transmission => end_transmission,
						mosi => JA(1),
						sclk => JA(3)
			);

			--  Assign slave select output
			JA(0) <= slave_select;


end Behavioral;

