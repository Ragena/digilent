----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:59:59 04/19/2013 
-- Design Name: 
-- Module Name:    DutyCycleDef - package 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package DutyCycleDef is

constant duty_min : integer := 100000;
constant duty_max : integer := 300000;

end DutyCycleDef;

package body DutyCycleDef is

end DutyCycleDef;

