----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:53:08 04/19/2013 
-- Design Name: 
-- Module Name:    AxisControll - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--use DutyCycleDef.all;

entity AxisControll is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           duty_in : in  integer;
           Axis_data : in  STD_LOGIC_VECTOR (15 downto 0);
           LeftPWM : out  STD_LOGIC;
           RightPWM : out  STD_LOGIC);
end AxisControll;

architecture Behavioral of AxisControll is

	COMPONENT PulseWidth
	PORT(
		clk : IN std_logic;
		RST : IN std_logic;
		Duty_in : IN integer;
		PWM : OUT std_logic
		);
	END COMPONENT;

constant duty_min : integer := 100000;
constant duty_max : integer := 300000;
constant step_divisor : integer := 2048;
constant step_multiplier : integer := 2;
--constant negative_begin : std_logic_vector := X"8000";


signal d_duty : integer;
signal duty_step : integer;
signal leftDuty, rightDuty : integer range duty_min to duty_max;

begin

duty_step <= 1 when (Duty_in - duty_min)/step_divisor = 0 else
	((Duty_in - duty_min)/step_divisor)*step_multiplier;
d_duty <= duty_step * conv_integer(Axis_data);
leftDuty <= duty_in - d_duty;
rightDuty <= duty_in + d_duty;

	leftMotor : PulseWidth PORT MAP(
		clk => clk,
		PWM => leftPWM,
		RST => rst,
		Duty_in => leftDuty
	);
	
	rightMotor : PulseWidth PORT MAP(
		clk => clk,
		PWM => rightPWM,
		RST => rst,
		Duty_in => rightDuty
	);

end Behavioral;

